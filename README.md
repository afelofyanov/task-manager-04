# TASK MANAGER

## DEVELOPER INFO

* **NAME**: Alexander Felofyanov
* **E-MAIL**: afelofyanov@t1-consulting.ru
* **E-MAIL**: afelofyanov@vtb.ru

## SOFTWARE

* JDK 8

* Intellij Idea

* MS Windows 10 Корпоративная Версия 20H1

## HARDWARE

* **RAM**: 8Gb

* **CPU**: i5

* **SSD**: 256Gb

## RUN PROGRAM

```shell
java -jar ./task-manager.jar
```
